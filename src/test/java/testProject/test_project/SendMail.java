package testProject.test_project;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.nio.file.Path;
import java.nio.file.Paths;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SendMail {
	private WebDriver driver;

	@Before
	public void before() {

		// set drivers
		Path resourceDirectory = Paths.get("src/resources/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", resourceDirectory.toString());
		driver = new ChromeDriver();

		driver.get("http://www.gmail.com");
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.id("view_container")));

		// login
		WebElement emailInput = driver.findElement(By.id("identifierId"));
		emailInput.sendKeys("thismail4mywork@gmail.com");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement nextButton = driver.findElement(By.id("identifierNext"));
		nextButton.click();

		new WebDriverWait(driver, 10).until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("(//DIV[@class='aCsJod oJeWuf'])[1]//INPUT[@type='password']")));

		WebElement passwordInput = driver
				.findElement(By.xpath("(//DIV[@class='aCsJod oJeWuf'])[1]//INPUT[@type='password']"));

		passwordInput.sendKeys("simplePassword11");

		WebElement nextStep = driver.findElement(By.id("passwordNext"));
		nextStep.click();

	}

	@Given("^User open email form$")
	public void userOpenEmailForm() {
		WebElement composeButton = driver.findElement(By.xpath("//DIV[text()='COMPOSE']"));
		new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//DIV[text()='COMPOSE']")));
		composeButton.click();
	}

	@When("^User enter (.+?) and (.+?) into fields$")
	public void userEnterEmailIntoRecepintField(String email, String message) {
		WebElement emailInput = driver.findElement(By.xpath("//TEXTAREA[@aria-label='To']"));
		emailInput.sendKeys(email);

		WebElement messageTextarea = driver.findElement(By.xpath("//DIV[@aria-label='Message Body']"));
		messageTextarea.sendKeys(message);

	}

	@And("^Choose virtual keybord from menu$")
	public void chooseVirtualKeybordFromMenu() {

		driver.findElement(By.xpath("//A[@data-tooltip='Input tools on/off (Ctrl-Shift-K)']")).click();

		WebElement keybord = driver.findElement(By.id("kbd"));

		assertTrue(keybord.isDisplayed());
	}

	@And("^Write email subject$")
	public void writeEmailSubject() {

		// focus on subject
		driver.findElement(By.xpath("//INPUT[@name='subjectbox']")).click();

		// try to write hello
		driver.findElement(By.xpath("//DIV[@id='kbd']//SPAN[text()='h']")).click();
		driver.findElement(By.xpath("//SPAN[text()='e']")).click();
		driver.findElement(By.xpath("//SPAN[text()='l']")).click();
		driver.findElement(By.xpath("//SPAN[text()='l']")).click();
		driver.findElement(By.xpath("//SPAN[text()='o']")).click();

	}

	@Then("^User send email$")
	public void userSendEmail() {

		driver.findElement(By.xpath("//DIV[text()='Send']")).click();

		try {
			// if showing system confirm
			Alert alert = driver.switchTo().alert();
			alert.accept();
			// alert title
			WebElement title = driver.findElement(By.xpath("//DIV[@class='Kj-JD']//SPAN[@role='heading']"));
			Boolean isError = title.getText().contains("Error");

			assertFalse(isError);
		} catch (NoAlertPresentException e) {
			// That's fine.
		}
	}

	@After
	public void after() throws InterruptedException {
		driver.quit();
	}

}
