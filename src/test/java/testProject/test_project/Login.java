package testProject.test_project;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

//import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.nio.file.Path;
import java.nio.file.Paths;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

public class Login {

	private WebDriver driver;

	@Before
	public void before() {
		// Path resourceDirectory = Paths.get("src/resources/geckodriver.exe");
		// System.setProperty("webdriver.gecko.driver", resourceDirectory.toString());
		// driver = new FirefoxDriver();

		Path resourceDirectory = Paths.get("src/resources/chromedriver.exe");

		System.setProperty("webdriver.chrome.driver", resourceDirectory.toString());

		driver = new ChromeDriver();

//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://www.gmail.com");
	}

	@Given("^User navigate to login form$")
	public void userNavigateToLoginForm() {
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.id("view_container")));
	}

	@When("^User enter (.+?) into email field and click next$")
	public void userEnterEmailIntoFieldAndClickNext(String email) {

		WebElement emailInput = driver.findElement(By.id("identifierId"));
		emailInput.sendKeys(email);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement nextButton = driver.findElement(By.id("identifierNext"));
		nextButton.click();

		Object isInvalid = emailInput.getAttribute("aria-invalid");
		assertNull(isInvalid);
	}

	@And("^User enter (.+?) into password field and click next$")
	public void userEnterPasswordIntoFieldAndClickNext(String password) {
		WebElement passwordInput = driver
				.findElement(By.xpath("(//DIV[@class='aCsJod oJeWuf'])[1]//INPUT[@type='password']"));

		passwordInput.sendKeys(password);

		WebElement nextButton = driver.findElement(By.id("passwordNext"));
		nextButton.click();
		Object isInvalid = passwordInput.getAttribute("aria-invalid");

		assertNull(isInvalid);
	}

	@Then("^User will be redirected to dashboard$")
	public void userWillBeRedirectedToDashboard() {
		WebElement titleElement = driver.findElement(By.xpath("//title"));
		Boolean isDashboard = titleElement.getText().contains("Inbox");

		assertFalse(isDashboard);
	}
	
		
	
	

	@After
	public void after() throws InterruptedException {
		driver.quit();
	}

}