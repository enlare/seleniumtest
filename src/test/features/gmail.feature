@workWithGmail
Feature: Login on gmail.com and send email. For sending e-mail from gmail.com user should be login with correct email and password

  @login
  Scenario Outline: Open form and log in
    Given User navigate to login form
    When User enter <email> into email field and click next
    And User enter <password> into password field and click next
    Then User will be redirected to dashboard

    Examples: 
      | email                     | password         | status  |
      | thismail4mywork@gmail.com | simplePassword11 | success |
      | linn.coffeein@gmail.com   |              123 | Fail    |

  @sendEmail
  Scenario Outline: Open message form and enter recepient email address, subject and text, useing virtual keybord for subject.
    Given User open email form
    When User enter <email> and <message> into fields
    And Choose virtual keybord from menu
    And Write email subject
    Then User send email

    Examples: 
      | email                   | message      | status  |
      | linn.coffeein@gmail.com | message text | success |
      | sdfsm                   | message text | fail    |
